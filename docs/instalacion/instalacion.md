Para utilizar Electron debemos tener instalado previamente NodeJS, integrado con un gestionador de dependencias como NPM.

Para instalar los binarios de Electron usamos `npm`. 

```
# Instalar el comando electron global y su uso desde el cli
npm install electron -g
# Instalar dependencias de desarrollo
npm install electron --save-dev
```

Ahora que ya tenemos Electron instalado podemos compobrar la versión por ejemplo:

```
C:\Users\i.landajuela>electron -v
v3.0.5
```

Si ejecutamos el comando sin argumentos abre una ventana por defecto con información y recursos.

![](img/01.png)

Podemos ejecutar un proyecto llamando a su main.js usando Electron `electron main.js`.


