[TOC]

# Enlaces

## Electron oficial 

* Sigue a [@ElectronJS](https://twitter.com/electronjs) en Twitter.
* [electronjs.org/docs](https://electronjs.org/docs) - Documentación oficial. 
* [electron/fiddle](https://github.com/electron/fiddle) - La forma más sencilla de empezar, es una herramienta para construir y ejecutar aplicaciones Electron.
* [Electron Userland](https://electronjs.org/userland): Compendio de paquetes y aplicaciones de la gente.

# Aplicaciones 

## Electron Fiddle

[Electron Fiddle](https://github.com/electron/fiddle/) es un entorno para desarrollar y probar proyectos Electron.

![](img/01.png)

# Ebooks

## Cross-Platform Desktop Applications Using Node, Electron, and NW.js - Paul B. Jensen

![](https://images.manning.com/720/960/resize/book/7/d385764-cf56-4a45-ac2e-59e032425772/Jensen-CPDA-HI.png)

* [Versión navegable en línea del libro](https://livebook.manning.com/#!/book/cross-platform-desktop-applications/about-this-book/)
* [Ejemplos del código en GitHub](https://github.com/paulbjensen/cross-platform-desktop-applications)

# Otras aplicaciones

* Cliente Git 
* Editor Notepad++

